#Explanation
This is an expendable repository for Ice House's Git flow training. It's a modified version of [Vincent Driessen's successful git branching model.](http://nvie.com/posts/a-successful-git-branching-model/)

#How to use
1. Open the package box.
2. Charge the batteries for 2 hours.
3. Playing it gently.
4. Recharge after 8 hours of use.
5. Keep away from children.

#License
This product is under LGPL license. The GNU Lesser General Public License (LGPL) is a free software license published by the Free Software Foundation (FSF).